import { VueEditor } from 'vue2-editor'

export default {
  name: 'FCreateArticle',
  components: {
    VueEditor
  },
  data: () => ({
    valid: false,
    name: '',
    nameRules: [
      v => !!v || 'Name is required',
      v => v.length <= 10 || 'Name must be less than 10 characters'
    ],
    content: ''
  }),
  methods: {
    clear () {
      this.$refs.form.reset()
    }
  }
}
