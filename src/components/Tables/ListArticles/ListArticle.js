export default {
  name: 'ListArticle',
  data () {
    return {
      headers: [
        {
          text: 'Title',
          align: 'left',
          sortable: false
        },
        { text: 'Actions',
          align: 'left',
          sortable: false
        }
      ],
      articles: [
        {
          name: 'Article 1'
        },
        {
          name: 'Article 2'
        }
      ]
    }
  }
}
