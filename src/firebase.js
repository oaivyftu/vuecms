import firebase from 'firebase'
import 'firebase/firestore'

var config = {
  apiKey: "AIzaSyCsHaIUuwUk_zV9J4CGgoL6z7QlvJqLsmE",
  authDomain: "vuecms-f9491.firebaseapp.com",
  databaseURL: "https://vuecms-f9491.firebaseio.com",
  projectId: "vuecms-f9491",
  storageBucket: "vuecms-f9491.appspot.com",
  messagingSenderId: "155009562248"
}

const firebaseApp = firebase.initializeApp(config)

const firestore = firebaseApp.firestore()

firestore.settings({ timestampsInSnapshots: true })

export default firestore