import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import CreateArticle from './views/CreateArticle.vue'
import EditArticle from './views/EditArticle.vue'
import DemoTodoApp from './views/DemoTodoApp.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      meta: {
        title: 'Home Page - VueCMS'
      }
    },
    {
      path: '/todo',
      name: 'to-do',
      component: DemoTodoApp,
      meta: {
        title: 'DemoTodo - VueCMS'
      }
    },
    {
      path: '/edit-article',
      name: 'edit-article',
      component: EditArticle,
      meta: {
        title: 'Edit article - VueCMS'
      }
    },
    {
      path: '/create-article',
      name: 'create-article',
      component: CreateArticle,
      meta: {
        title: 'Create article - VueCMS'
      }
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
      meta: {
        title: 'About - VueCMS'
      }
    }
  ]
})
